<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Does the actual API calls having been given the information to send to the SMSJuice HTTP API
 *
 * @author Wilson Mazaiwana
 */

namespace SMSJuiceAPI;

Use SMSJuiceAPI\Objects\Message;
Use SMSJuiceAPI\Objects\ContactGroup;

class TextMessagingService {
    
    const API_URL = "https://api.smsjuice.com";
    
    const SEND_URI = "/send";
    const GROUPS_URI = "/groups";
    const CONTACTS_URI = "/groups/contacts";
    const GET_ALL_GROUPS_URI = "/groups/all";
    const GET_ALL_GROUP_CONTACTS_URI = "/groups/contacts/all";
    const GET_BALANCE = "/balance";
    const GET_CREDIT = "/credit";
    const GET_MESSAGE_REPORT = "/query";
    
    /**
     * Returns the amount of credit left on the account
     * @param type $key API Key found in the settings section of your account
     * @param type $secret API Secret found in the settings section of your account
     * @return String JSON with the value of credit left
     */
    public static function getCredit($key,$secret){
        return self::Get(self::GET_CREDIT, [$key, $secret]);
    }

    
    /**
     * Returns the cash balance on your account in your registered currency
     * @param type $key API Key found in the settings section of your account
     * @param type $secret API Secret found in the settings section of your account
     * @return String JSON with the balance value
     */
    public static function getBalance($key,$secret){
        return self::Get(self::GET_BALANCE, [$key, $secret]);
    }
    
    
    /**
     * Returns a list of all the contact groups on your account
     * @param type $key API Key found in the settings section of your account
     * @param type $secret API Secret found in the settings section of your account
     * @return String JSON with the list of all the contact groups on your account
     */
    public static function getAllGroups($key,$secret){
        return self::Get(self::GET_ALL_GROUPS_URI, [$key, $secret]);
    }
    
    
    /**
     * Returns all the contacts in a given group. It returns them with contact name and number.
     * @param type $key API Key found in the settings section of your account
     * @param type $secret API Secret found in the settings section of your account
     * @param type $groupName The name of the group whose contacts are to be returned
     * @return String JSON with all the names and numbers in the group queried
     */
    public static function getAllGroupContacts($key, $secret, $groupName){
        return self::Get(self::GET_ALL_GROUP_CONTACTS_URI, [$key, $secret, $groupName]);
    }
    
    
    /**
     * Deletes a group on your account on SMSJuice
     * @param type $key API Key found in the settings section of your account
     * @param type $secret API Secret found in the settings section of your account 
     * @param type $groupName Name of the group to be deleted
     * @return String JSON string with the final status of the delete task.
     */
    public static function deleteGroup($key, $secret, $groupName){
        return self::Delete(self::GROUPS_URI, [$key, $secret, $groupName]);
    }
    
    
    /**
     * Delete a contact from a contacts group
     * @param type $key API Key found in the settings section of your account
     * @param type $secret API Secret found in the settings section of your account
     * @param type $groupName Name of the group from which the contact will be deleted
     * @param type $contactNumber Number of the contact to be deleted
     * @return String JSON string with the final status of the delete task.
     */
    public static function deleteContactFromGroup($key, $secret, $groupName, $contactNumber){
        return self::Delete(self::CONTACTS_URI, [$key, $secret, $groupName, $contactNumber]);
    }
    
    
    /**
     * Send a message to the intended recipients through the SMSJuice HTTP API
     * @param Message $message Message object containing all the information for sending to the API
     * @return String JSON with the final status of the sending task
     */
    public static function sendMessage(Message $message){
        return self::Post($message->toJSON(), self::SEND_URI);
    }
    
    
    /**
     * Returns a message report detailing the sending times, delivery times, and delivery status of a message
     * @param type $key API Key found in the settings section of your account
     * @param type $secret API Secret found in the settings section of your account
     * @param type $messageId Message ID of the message for which a report is required
     * @return String JSON report showing the message sending and delivery status
     */
    public static function getMessageReport($key, $secret, $messageId){
        return self::Get(self::GET_MESSAGE_REPORT, [$key, $secret, $messageId]);
    }
    
    
    /**
     * Add a ne contacts group onto one's SMSJuice account, just as one would if they were to login on our web interface
     * @param ContactGroup $contactGroup ContactGroup object containing the group name, contact numbers (if any) and credentials to add the group
     * @return String JSON showing the final status of the addGroup operation
     */
    public static function addGroup(ContactGroup $contactGroup){
        return self::Post($contactGroup->toJSON(), self::GROUPS_URI);
    }
    
    
    /**
     * Add contacts (names & numbers OR just a numbers, or both) to an existing group on your SMSJuice account
     * @param ContactGroup $contactGroupToAddTo ContactGroup object containing the contacts to add to the existing group
     * @return String JSON showing the final status of the addContactToGroup operation
     */
    public static function addContactToGroup(ContactGroup $contactGroupToAddTo){
        return self::Post($contactGroupToAddTo->toJSON(), self::CONTACTS_URI);
    }


    private static function Post($json, $endpoint){
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => $json,
                'ignore_errors' => TRUE
            )
        );
        $context  = stream_context_create($opts);
        return file_get_contents(self::API_URL.$endpoint, false, $context);
    }
    
    
    private static function Get($endpoint, $path_variables=false){
        $path = "";
        if($path_variables){
            $path = implode("/", $path_variables);
        }
                $opts = array('http' =>
            array(
                'method'  => 'GET',
                'ignore_errors' => TRUE
            )
        );
        return file_get_contents(self::API_URL.$endpoint."/{$path}");
    }
    
    
    private static function Delete($endpoint, $path_variables=false){
        $path = "";
        if($path_variables){
            $path = implode("/", $path_variables);
        }
        $opts = array('http' =>
            array(
                'method'  => 'DELETE',
                'ignore_errors' => TRUE
            )
        );
        $context  = stream_context_create($opts);
        return file_get_contents(self::API_URL.$endpoint."/{$path}", false, $context);
    }
    

        
    
}