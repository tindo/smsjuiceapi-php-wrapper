<?php

spl_autoload_register(function ($full_class_name) {
    if(strpos($full_class_name, "\\")){
        $class_name_array = explode("\\", $full_class_name);
        $class_name = $class_name_array[count($class_name_array)-1];
    }else{
        $class_name = $full_class_name;
    }
    include("../Objects/{$class_name}.php");
    include("../TextMessagingService.php");
});

use SMSJuiceAPI\Objects\Message;
use SMSJuiceAPI\TextMessagingService;
$m2 = new Message("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe");
//sets the sender ID
$m2->setSender("TEST");

//sets the receiver ID
$m2->addToRecipientList("447900385586");

//sets the message
$m2->setMessage("Hello World! @ £ € $");


echo "\n\n";
echo $m2->toJSON();

echo "\n\n";
print_r(TextMessagingService::sendMessage($m2));


/**
$m = new Message("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe");
//sets the sender ID
$m->setSender("Nyaradzo");

$m->setRecipientType("groups");
//sets the receiver ID
$m->addToRecipientList("JandD");
//works, splits at API


$m->setMessage("Hello, this is a test message from ".basename($_SERVER['PHP_SELF']).". You can change the contents on line ".__LINE__.".".
        "Hello, this is a test message from ".basename($_SERVER['PHP_SELF']).". You can change the contents on line ".__LINE__.".");
//echo "\nSET MESSAGE LONGER THAN 160 CHARS";


echo "\n\n";
echo $m->toJSON();

echo "\n\n";
print_r(TextMessagingService::sendMessage($m));
*/