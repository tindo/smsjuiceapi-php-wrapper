<?php
require(__DIR__.'/../../vendor/autoload.php');
require_once(__DIR__."/TextMessagingServiceProxy.php");

/**
 * TextMessagingServiceTest Probably a futile test, as testing any other API client. Integration tests fare better
 * Having to use a proxy to mock static methods. Quite silly to be honest, but has to be done.
 *
 * @author Wilson Mazaiwana
 */


class TextMessagingServiceTest extends PHPUnit_Framework_TestCase {
    
        protected $client;
        private $textMessagingService;
        private $expectedPostResponseJSON;
        private $requestJson;
    
	protected function setUp() {
            $this->requestJson = <<<POST
                    {
                "key":"kei",
                "secret":"seikret",
                "from":"mock",
                "to":[
                "447900382686"
                ],
                "message":"Test message"
            }
POST;
            
            $this->expectedPostResponseJSON = <<<POST
                \{
                "submit": "success"
                "error": ""
                "total_recipients": 1
                "messages_used_per_recipient": 1
                "message": "Test message"
                "your_reference": ""
                "from": "ratings"
                "info": ""
                "message_query_ids": [1]
                0:  {
                "to": "447900382686"
                "message_id": "cltishnop1712k4s"
                }
                "invalid_numbers": [0]
                "schedule": "2016-08-30 15:56"
                "expiry": "2016-09-01 15:56"
                }
POST;

            
            $key = "kei";
            $secret = "seikret";
            
            $this->textMessagingService = $this->getMockBuilder('TextMessagingServiceProxy')
                    ->getMock();
            
            $this->textMessagingService->method('getProxy')
                    ->with($key, $secret)
                    ->will($this->returnValue("{'Balance':0.00}"));
            
            $this->textMessagingService->method('postProxy')
                    ->with($this->requestJson)
                    ->will($this->returnValue($this->expectedPostResponseJSON));
            
            $this->textMessagingService->method('deleteProxy')
                    ->with($key, $secret,"groupToDelete")
                    ->will($this->returnValue("{\"operation\":\"successful\"}"));
	}

        
        public function testGet(){
            $response = $this->textMessagingService->getProxy("kei","seikret");
            $data = json_decode($response, true);
            $this->assertEquals(0.00, $data['Balance']);
            
        }
        
        public function testPost(){

            $response = $this->textMessagingService->postProxy($this->requestJson);
            $data = json_decode($response);
            $expectedData = json_decode($this->expectedPostResponseJSON);
            $this->assertEquals($expectedData['message_id'], $data['message_id']);
        }
        
        public function testDelete(){
            $response = $this->textMessagingService->deleteProxy("kei","seikret","groupToDelete");
            $data = json_decode($response,true);
            $this->assertEquals("successful", $data['operation']);
        }
        
        	protected function tearDown() {
	}
}
