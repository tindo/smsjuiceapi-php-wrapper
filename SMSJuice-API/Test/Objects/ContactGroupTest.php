<?php
require_once(__DIR__."/../../Objects/ContactGroup.php");

/**
 * Tests the Contactgroup object
 *
 * @author Wilson Mazaiwana
 */

use SMSJuiceAPI\Objects\ContactGroup;


class ContactGroupTest extends PHPUnit_Framework_TestCase {


    public function setUp() {
    }

    public function tearDown() {
    }

    public function testCreateNewContactGroupWithNullValuesThrowsException(){
        //null values
        try{
            $c = new ContactGroup(null, null, null);
            $this->fail("Needs fixing");
        }catch(Exception $e){
            //expected
        }
    }    
        
    public function testCreateNewGroupWithEmptyValuesThrowsException(){
        //empty values
        try{
            $c = new ContactGroup("", "", "");
            $this->fail("Needs fixing");
        }catch(Exception $e){
            //expected
        }
    }

    public function testCreateNewGroupWithUninitialisedValuesThrowsException(){
        //unset/uninitialised values
        try{
            $c = new ContactGroup($ky, $se, "name");
            $this->fail("Needs fixing");
        }catch(Exception $e){
            //expected       
        }
    }    
    
    public function testCreateNewGroupWithSetValuesIsOk(){
        try{
            $c = new ContactGroup("kei", "seikret", "naim");
        }catch(Exception $e){
            $this->fail("Needs fixing"); //unexpected
        }
    }
    
    public function testAddValidContactToCreatedGroup(){
        try{
            $c = new ContactGroup("kei", "seikret", "naim");
            $c->addContact("447900368894");
            $group_properties_array = json_decode($c->toJSON(),true);
            $this->assertTrue(in_array("447900368894", $group_properties_array['contacts']));
        }catch(Exception $e){
            $this->fail("Needs fixing"); //unexpected
        }
    }
    
    public function testAddValidContactListToCreatedGroup(){
        try{
            $c = new ContactGroup("kei", "seikret", "naim");
            $c->addContactList(["447900368894","496655664589","James Bond"=>"447901901007"]);
            $group_properties_array = json_decode($c->toJSON(),true);
            $this->assertTrue(count($group_properties_array['contacts'])===3);
        }catch(Exception $e){
            $this->fail($e->getMessage()); //unexpected
        }
    }
    
    public function testToJSON(){
        try{
            $c = new ContactGroup("kei", "seikret", "naim");
            $c->addContactList(["447900368894","496655664589","James Bond"=>"447901901007"]);
            $group_properties_array = json_decode($c->toJSON(),true);
            $properties_exist = array_key_exists("key", $group_properties_array) 
                    && array_key_exists("secret", $group_properties_array)
                    && array_key_exists("groupName", $group_properties_array)
                    && array_key_exists("contacts", $group_properties_array);
            $this->assertTrue($properties_exist);
        }catch(Exception $e){
            $this->fail($e->getMessage()); //unexpected
        }        
    }
    

}
