# SMSJuice HTTP API PHP Wrapper

SMSJuice HTTP API PHP Wrapper provides a clean way to connect to SMSJuice HTTP API servers. The main features in v1.0.0 are

  - Sending messages to single/multiple individual contacts
  - Sending messages to single/multiple groups on your SMSJuice account
  - Check for message reports for a certain message
  - Check account balance and/or remaining SMS credits
  - Add new groups with contacts to your SMSJuice account
  - Add new contacts (with names if needed) to existing groups on your SMSJuice account
  - Get a list of all the groups on your SMSJuice account
  - Get a list of all contacts from a given group on your SMSJuice account
  - Delete groups (with all the contacts contained in it )on your SMSJuice account
  - Delete contacts from a group on your SMSJuice account

## Installation
##### 1. Composer (Recommended way)
You can use Composer to install the SMSJuice HTTP API Wrapper. Composer is a tool for dependency management in PHP. It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.
To install composer follow instructions on https://getcomposer.org/doc/00-intro.md

After you install composer, use the following command in your commandline/shell to install the SMSJuice HTTP API wrapper. 

```sh
$ composer require smsjuice/http-api-wrapper
```
...And you're good to go!

##### 2. Download Zip
Download the Zip from https://www.smsjuice.com/text-messaging-api , and manually put it in a folder accessible from your project code.

## Usage
When you are using the composer package structure then below is an example of the you could do:
```sh

$m = new Message("key", "secret"); //new message object with your key and secret
 //FROM address        
$m->setSender("Restaurant");
 //Recipient Type. Options - 'groups' OR 'numbers'. Default:'numbers'
$m->setRecipientType("numbers");
$m->setRecipientList(array("447900239984", "447900392828")); //add contacts
$m->addToRecipientList("447900385486"); //add one contact
$m->setMessage("Hello world"); //content to send
//Send the message and get the response from the server
$result = TextMessagingService::sendMessage($m);
//JSON Result from the server
echo $result;
```
License
----

APACHE2



